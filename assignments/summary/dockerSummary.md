## Docker basics
### Why docker ?
It becomes hard to manage dependencies as we build large projects and if we want to deploy them in various environments. So we will be using docker to deploy our product. For this you don't need to master docker but need to know the basics.

## Docker Terminologies

1. Docker

Docker is a program for developers to develop, and run applications with containers.


2. Docker Image

* A Docker image is contains everything needed to run an applications as a container. This includes:

 * code
 * runtime
 * libraries
 * environment variables
 * configuration files


* The image can then be deployed to any Docker environment and as a container.


3. Container

* A Docker container is a running Docker image.
* From one image you can create multiple containers .


4. Docker Hub

Docker Hub is like GitHub but for docker images and containers.


#### Docker Installation
1. Learn to Install docker on to your Ubuntu 16.04.

Uninstall the older version of docker if is already installed

$ sudo apt-get remove docker docker-engine docker.io containerd runc

2. Installing CE (Community Docker Engine)

$ sudo apt-get update
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo apt-key fingerprint 0EBFCD88
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable nightly test"
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-ce-cli containerd.io

// Check if docker is successfully installed in your system
$ sudo docker run hello-world

## Docker basic commands
Learn basic docker commands

## docker ps

The docker ps command allows us to view all the containers that are running on the Docker Host.

for Example:
$ docker ps

CONTAINER ID IMAGE  COMMAND CREATED        STATUS            PORTS NAMES
30986b73dc00 ubuntu "bash"  45 minutes ago Up About a minute                 elated_franklin

## docker start

This command starts any stopped container(s).
Example

$ docker start 30986
In the above example, Docker starts the container beginning with the container ID 30986.
$ docker start sneha
Whereas in this example, Docker starts the container named sneha.

## docker stop

This command stops any running container(s).
Example

$ docker stop 30986
In the above example, Docker will stop the container beginning with the container ID 30986.
$ docker stop sneha
Whereas in this example, Docker will stop the container named sneha.

## docker run

This command creates containers from docker images.
Example

$ docker run sneha

## docker rm

This command deletes the containers.
Example

$ docker rm sneha

### Common Operations on Dockers
Typically, you will be using docker in the given flow.

1. Download/pull the docker images that you want to work with.
2.   Copy your code inside the docker
3. Access docker terminal
4. Install and additional required dependencies
5. Compile and Run the Code inside docker
6. Document steps to run your program in README.md file
7. Commit the changes done to the docker.
8. Push docker image to the docker-hub and share repository with people who want to try your code.


Example of the above flow:
1. Download the docker.
docker pull snehabhapkar/trydock
2. Run the docker image with this command.
docker run -ti snehabhapkar/trydock /bin/bash
Which will output like this, this means that now you are running the docker container and you are inside the container.
root@e0b72ff850f8:/#
Every running container has an ID in your system. Container ID in my case is e0b72ff850f8.
3. Let copy our code inside docker with this command. (Make sure you are not inside docker terminal, enter exit in command line to get out of container terminal)

 * In this case, lets copy a simple python program which just prints "hello, I am talking from container". So lets consider you have this file hello.py

print("hello, I am talking from container")

 * Copy file inside the docker container

docker cp hello.py e0b72ff850f8:/
where e0b72ff850f8 is the containerID. This will copy hello.py inside docker in root directory.

 * All write a script for installing dependencies - requirements.sh

apt update
apt install python3

* Copy file inside docker

docker cp requirements.sh e0b72ff850f8:/
4. Install dependencies
If you want to install additional dependencies then you can, write all the steps for installation in a shell script and run the script inside the container.

 * Give permission to run shell script

docker exec -it e0b72ff850f8 chmod +x requirements.sh

 * Install dependencies

docker exec -it e0b72ff850f8 /bin/bash ./requirements.sh
5. Run the program inside container with this command.
docker start e0b72ff850f8
docker exec e0b72ff850f8 python3 hello.py
where e0b72ff850f8 is the containerID.
6. Save your copied program inside docker image with docker commit.
docker commit e0b72ff850f8 snehabhapkar/trydock
where e0b72ff850f8 is the containerID.
7. Push docker image to the dockerhub.

 * Tag image name with different name

docker tag snehabhapkar/trydock username/repo
where username is your username on dockerhub and repo is the name of image.
For example:
docker tag snehabhapkar/trydock yh42/trial-dock-img

 * Push on dockerhub

docker push username/repo